package net.therap.LogParser;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by nourin on 11/3/16.
 */
public class LogParserSummaryView {

    private ArrayList<LogFileModel> logFileModels;
    private ArrayList<LogSummary> logSummaryList;
    private boolean toSort;

    public LogParserSummaryView(ArrayList<LogFileModel> logFileModels, boolean toSort) {

        this.logFileModels = logFileModels;
        this.toSort = toSort;
        summarize();

    }

    private void summarize() {

        logSummaryList = new ArrayList<>();
        String currentTime = logFileModels.get(0).getTime();
        int currentHour = Integer.parseInt(currentTime.substring(0, 2));
        LogSummary logSummary = new LogSummary();

        for (int i = 0; i < logFileModels.size(); i++) {

            String time = logFileModels.get(i).getTime();
            int hour = Integer.parseInt(time.substring(0, 2));

            if (hour == currentHour) {

                if (logFileModels.get(i).hasDebug()) {

                    if (logFileModels.get(i).getDebug().isGet()){
                        logSummary.setGetRequestNumber(logSummary.getGetRequestNumber() + 1);}

                    if (logFileModels.get(i).getDebug().isPost()){
                        logSummary.setPostRequestNumber(logSummary.getPostRequestNumber() + 1);}

                    logSummary.getUniqueURIHashSet().add(logFileModels.get(i).getDebug().getUri());
                    logSummary.setTotalResponseTime(logSummary.getTotalResponseTime() + logFileModels.get(i).getDebug().getResponseTime());

                }
            } else {

                logSummary.setStartHour(currentHour);
                logSummary.setEndHour(currentHour + 1);
                logSummaryList.add(logSummary);
                logSummary = new LogSummary();
                time = logFileModels.get(i).getTime();
                hour = Integer.parseInt(time.substring(0, 2));
                currentHour = hour;
                i--;

            }
        }

        logSummary.setStartHour(currentHour);
        logSummary.setEndHour(currentHour + 1);
        logSummaryList.add(logSummary);
        if(toSort) Collections.sort(logSummaryList);
    }


    public void displaySummary() {

        System.out.println("Time               Get/POST Count          Unique URI Count          Total Response Time");
        if (logSummaryList == null) System.out.println(" logSummaryListNULL");
        for (int i = 0; i < logSummaryList.size(); i++) {

            if (logSummaryList.get(i) == null) System.out.println(" NULL");
            System.out.println(logSummaryList.get(i).toString());

        }
    }
}