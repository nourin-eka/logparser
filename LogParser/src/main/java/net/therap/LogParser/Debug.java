package net.therap.LogParser;

/**
 * Created by nourin on 11/2/16.
 */
public class Debug {

    private String uri;
    private boolean get;
    private boolean post;
    private int responseTime;
    private Parser parser;

    public Debug(String debug) {
        parser = new Parser();
        setResponseTime(parser.parseResponseTime(debug));
        setGet(parser.isGet(debug));
        setPost(parser.isPost(debug));
        setUri(parser.parseURI(debug));
    }


    public int getResponseTime() {
        return responseTime;
    }

    public void setResponseTime(int responseTime) {
        this.responseTime = responseTime;
    }

    public boolean isPost() {
        return post;
    }

    public void setPost(boolean post) {
        this.post = post;
    }

    public boolean isGet() {
        return get;
    }

    public void setGet(boolean get) {
        this.get = get;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

}
