package net.therap.LogParser;

import java.util.HashSet;

/**
 * Created by nourin on 11/3/16.
 */
public class LogSummary implements Comparable<LogSummary>{

    private int getRequestNumber;
    private int postRequestNumber;
    private HashSet<String> uniqueURIHashSet;
    private int totalResponseTime;
    private int startHour;
    private int endHour;
    private int totalResponseCount;
    private static final int ZERO = 0;
    private static final int TWELVE = 12;

    public LogSummary() {
        this.getRequestNumber = 0;
        this.postRequestNumber = 0;
        this.uniqueURIHashSet = new HashSet<String>();
        this.totalResponseTime = 0;
    }

    public int getGetRequestNumber() {
        return getRequestNumber;
    }

    public void setGetRequestNumber(int getRequestNumber) {
        this.getRequestNumber = getRequestNumber;
    }

    public int getPostRequestNumber() {
        return postRequestNumber;
    }

    public void setPostRequestNumber(int postRequestNumber) {
        this.postRequestNumber = postRequestNumber;
    }

    public int getUniqueURInumber() {

        return getUniqueURIHashSet().size();
    }

    public HashSet<String> getUniqueURIHashSet() {
        return uniqueURIHashSet;
    }

    public void setUniqueURIHashSet(HashSet<String> uniqueURIHashSet) {
        this.uniqueURIHashSet = uniqueURIHashSet;
    }

    public int getTotalResponseTime() {
        return totalResponseTime;
    }

    public void setTotalResponseTime(int totalResponseTime) {
        this.totalResponseTime = totalResponseTime;
    }


    public int getEndHour() {
        return endHour;
    }

    public void setEndHour(int endHour) {
        this.endHour = endHour;
    }

    public int getStartHour() {
        return startHour;
    }

    public void setStartHour(int startHour) {
        this.startHour = startHour;
    }

    public int getTotalResponseCount() {
        return getGetRequestNumber() + getPostRequestNumber();
    }

    private String timeFormat() {

        String time = "";
        String am = "am";
        String pm = "pm";

        if (getStartHour() >= ZERO && getStartHour() < TWELVE) {
            time = String.format("%d.00%s - ", getStartHour(), am);
        } else if (getStartHour() >= TWELVE) {
            time = String.format("%d.00%s - ", 24 - getStartHour(), pm);
        }

        if (getEndHour() >= ZERO && getEndHour() < TWELVE) {
            time = time + String.format("%d.00%s", getEndHour(), am);
        } else if (getEndHour() >= TWELVE) {
            time = time + String.format("%d.00%s", 24 - getEndHour(), pm);
        }

        return time;
    }
    public int compareTo(LogSummary logSummary) {

        if (logSummary == null) {
            throw new NullPointerException("Attempted to compare " + this + " to null");
        }
        return logSummary.getTotalResponseCount() - this.getTotalResponseCount();
    }

    @Override
    public String toString() {

        return timeFormat() + "         " + getRequestNumber + "/" + postRequestNumber + "                       " + getUniqueURInumber() + "                     " + totalResponseTime;
    }
}