package net.therap.LogParser;

/**
 * Created by nourin on 11/3/16.
 */
public class LogParser {

    private static LogParserController logParserController;
    private static final String defaultFileName = "src/main/resources/sample-log-file";
    public static void main(String[] args) {

        logParserController = new LogParserController();
        if (args.length > 0) {
            logParserController.initializeLogModels(args[0]);
        } else {
            logParserController.initializeLogModels(defaultFileName);
        }
        logParserController.viewLogSummary(args);
    }
}