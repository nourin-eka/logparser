package net.therap.LogParser;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by nourin on 11/2/16.
 */
public class LogParserController {

    private ArrayList<LogFileModel> logFileModelArrayList;
    private LogParserSummaryView logParserSummaryView;
    private final String sortCommand = "--sort";

    public void initializeLogModels(String fileName) {

        String line;
        logFileModelArrayList = new ArrayList<>();
        LogFileModel logFileModel;

        try {

            FileReader fileReader = new FileReader(fileName);
            BufferedReader bufferedReader = new BufferedReader(fileReader);

            while ((line = bufferedReader.readLine()) != null) {

                logFileModel = new LogFileModel(line);
                logFileModelArrayList.add(logFileModel);

            }

        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        } catch (IOException ex) {
            System.out.println("Error reading file " + fileName);
        }
    }

    public void viewLogSummary(String[] args) {

        if (args.length > 1 && args[1].equals(sortCommand)) {
            logParserSummaryView = new LogParserSummaryView(logFileModelArrayList, true);
        } else {
            logParserSummaryView = new LogParserSummaryView(logFileModelArrayList, false);
        }
        logParserSummaryView.displaySummary();
    }
}