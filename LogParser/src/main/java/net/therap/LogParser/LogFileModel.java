package net.therap.LogParser;

/**
 * Created by nourin on 11/2/16.
 */
public class LogFileModel {

    private String date;
    private String time;
    private Debug debug;
    private String info;
    private Parser parser;
    private boolean hasDebug;
    private static final String DEBUG = "DEBUG";

    public LogFileModel(String log) {

        parser = new Parser();
        setDate(parser.parseDate(log));
        setHasDebug(false);

        if(log.contains(DEBUG)){
            setHasDebug(true);
            setDebug(new Debug(log));
        }

        setInfo(parser.parseINFO(log));
        setTime(parser.parseTime(log));

    }


    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public boolean hasDebug() {
        return hasDebug;
    }

    public void setHasDebug(boolean hasDebug) {
        this.hasDebug = hasDebug;
    }

    public Debug getDebug() {
        return debug;
    }

    public void setDebug(Debug debug) {
        this.debug = debug;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}