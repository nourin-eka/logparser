package net.therap.LogParser;

import java.util.regex.Pattern;
import java.util.regex.Matcher;

/**
 * Created by nourin on 11/2/16.
 */
public class Parser {

    private static final String DATE_PATTERN = "((19|20)\\d\\d)-(0?[1-9]|1[012])-(0?[1-9]|[12][0-9]|3[01])";
    private static final String PROFILER_PATTERN = "\\[PROFILER:\\d\\d\\d\\]";
    private static final String GET_PATTERN = "G,";
    private static final String POST_PATTERN = "P,";
    private static final String URI_PATTERN = "(URI\\=)(\\[(.*)\\])";
    private static final String RESPONSE_TIME_PATTERN = "(time\\=)([0-9]*)(ms)";
    private static final String INFO_PATTERN = "INFO.*";
    private static final String TIME_PATTERN = "[0-9][0-9]:[0-9][0-9]:[0-9,]*";
    private Pattern pattern;
    private Matcher matcher;

    public String parseDate(String str){
        Matcher matcher = findMatch(DATE_PATTERN, str);
        matcher.find();
        return matcher.group();
    }

    public boolean isProfiler(String str) {
        Matcher matcher = findMatch(PROFILER_PATTERN, str);
        return matcher.find();
    }

    public boolean isGet(String str){
        Matcher matcher = findMatch(GET_PATTERN, str);
        return matcher.find();
    }

    public boolean isPost(String str){
        Matcher matcher = findMatch(POST_PATTERN, str);
        return matcher.find();
    }

    public String parseURI(String str) {
        Matcher matcher = findMatch(URI_PATTERN, str);
        if(matcher.find()) {
            return matcher.group();
        }
        return null;
    }

    public int parseResponseTime(String str){
        Matcher matcher = findMatch(RESPONSE_TIME_PATTERN,str);
        if(matcher.find()){
            return  Integer.parseInt(matcher.group(2).toString());}
        return 0;
    }

    public String parseINFO(String str){
        Matcher matcher = findMatch(INFO_PATTERN,str);
        if(matcher.find()) {
            return matcher.group();
        }
      return null;
    }

    public String parseTime(String str) {
        Matcher matcher = findMatch(TIME_PATTERN,str);
        if(matcher.find()) {
            return matcher.group();
        }
        return null;
    }

    private Matcher findMatch(String regex, String str) {
        pattern = Pattern.compile(regex);
        return pattern.matcher(str);
    }
}